﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {

	public List<ParticleSystem> particles;

	// Use this for initialization
	public void ShowParticles(bool b){
		foreach (ParticleSystem p in particles)
		{
			p.gameObject.SetActive(b);
		}
	}

	public void PlayParticles(int index){
		
		particles[index].Play();
	
	}
}
