﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateImage : MonoBehaviour {

	public float Rate = 0f;

	// Update is called once per frame
	void Update () {
		Vector3 r = transform.rotation.eulerAngles;
		r.z += Rate;
		Quaternion q = transform.rotation;
		q.eulerAngles = r;
		transform.rotation = q;
	}
}
