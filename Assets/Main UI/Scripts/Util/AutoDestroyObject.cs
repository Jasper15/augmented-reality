﻿using UnityEngine;
using System.Collections;

public class AutoDestroyObject : MonoBehaviour {
	
	[SerializeField]
	float destroyDelay = 5;
	// Use this for initialization
	void Start () {
		StartCoroutine ("DestroySelf");
	}
	
	IEnumerator DestroySelf(){
		yield return new WaitForSeconds (destroyDelay);
		Destroy (gameObject);	
	}
		
	public static void AutoDestroy(GameObject g, float delay){
		AutoDestroyObject obj = g.AddComponent<AutoDestroyObject> ();
		obj.destroyDelay = delay;
	}
}
