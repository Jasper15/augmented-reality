﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class LoadMovieClip : MonoBehaviour {

	public string videoPath;
	// Use this for initialization
	void Awake () {
		VideoPlayer player = GetComponent<VideoPlayer>();
		VideoClip vid = Resources.Load<VideoClip>(videoPath);

		player.clip = vid;
	}
}
