﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

namespace LegoBatman.BTT{
	
	public class ScrollingUV : MonoBehaviour
	{


		public enum imageType
		{
			RawImage, Texture
		}

		public int materialIndex = 0;
		public Vector2 uvAnimationRate = new Vector2( 1.0f, 0.0f );
		public string textureName = "_MainTex";
		public Vector2 startPos;

		Vector2 uvOffset = Vector2.zero;

		public imageType type = imageType.Texture;

		Renderer renderer;
		RawImage image;
		Rect rect;

		public float multiplier;


		void Start(){

			if (type == imageType.Texture) {
				renderer = GetComponent<Renderer> ();
				startPos = renderer.materials[materialIndex].GetTextureOffset(textureName);
			}
			else {
				image = GetComponent<RawImage> ();
				rect = new Rect (0, 0, image.uvRect.width, image.uvRect.height);
				startPos = Vector2.zero;
			}
		}

		void LateUpdate()
		{


			uvOffset += ( uvAnimationRate * multiplier * Time.deltaTime );
			uvOffset.x -= (int)uvOffset.x;
			uvOffset.y -= (int)uvOffset.y;

			if (type == imageType.Texture) {
				renderer.materials [materialIndex].SetTextureOffset (textureName, uvOffset + startPos);
			}
			else {
				if( image.enabled )
				{
					rect.x = uvOffset.x;
					rect.y = uvOffset.y;
					image.uvRect = rect;
				}
			}


		}
	}
}