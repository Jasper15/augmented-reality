﻿using UnityEngine;
using System.Collections;

public static class DeviceRotation {
	private static bool gyroInitialized = false;

	public static bool HasGyroscope {
		get {
			return SystemInfo.supportsGyroscope;
		}
	}

	public static Quaternion Get() {
		if (!gyroInitialized) {
			InitGyro();
		}

		return HasGyroscope
			? ReadGyroscopeRotation()
				: Quaternion.identity;
	}

	private static void InitGyro() {
		if (HasGyroscope) {
			Input.gyro.enabled = true;                // enable the gyroscope
			Input.gyro.updateInterval = 0.0167f;    // set the update interval to it's highest value (60 Hz)
		}
		gyroInitialized = true;
	}

	private static Quaternion ReadGyroscopeRotation() {
		return new Quaternion(0.5f, 0.5f, -0.5f, 0.5f) * Input.gyro.attitude * new Quaternion(0, 0, 1, 0);
	}
}


public class GyroManager : MonoBehaviour {

	Quaternion initialRotation, gyroInitialRotation;

	public Transform world;


	IEnumerator Start ()
	{
		Input.compass.enabled = true;
		//Input.location.Start();

		//initialize compass
		while (initialRotation.eulerAngles == Vector3.zero && Time.timeSinceLevelLoad < 5)
		{
			yield return new WaitForEndOfFrame();


			Debug.Log("initializing compass");
			initialRotation = Quaternion.Euler(0, -Input.compass.trueHeading, 0);
#if !UNITY_ANDROID
			world.Rotate(initialRotation.eulerAngles);
#endif
			Input.compass.enabled = true;
		}

		//initialize gyro
		while (!Input.gyro.enabled )
		{
			Input.gyro.enabled = true;
			Input.gyro.updateInterval = 0.0167f;    // set the update interval to it's highest value (60 Hz)



			yield return new WaitForEndOfFrame();


			gyroInitialRotation = Quaternion.identity;// Input.gyro.attitude;


			Debug.Log("initializing gyro");

			Input.gyro.enabled = true;
		}


		//gyroInitialRotation = Input.gyro.attitude;
		//initialRotation = Quaternion.Euler(0, -Input.compass.magneticHeading, 0);



		Debug.Log("gyro and compass initialized: gyro: " + gyroInitialRotation.eulerAngles + ", compass: " + initialRotation.eulerAngles );
		initialized = true;

		yield break;
	}
	void OnGUIxxx()
	{
		
		try{
			GUILayout.Label ("Gyroscope attitude : " + Input.gyro.attitude);
			GUILayout.Label("Gyroscope gravity : " + Input.gyro.gravity);

	//		GUILayout.Label("Gyroscope rotationRate : " + Input.gyro.rotationRate);
	//		GUILayout.Label("Gyroscope rotationRateUnbiased : " + Input.gyro.rotationRateUnbiased);
			GUILayout.Label("Gyroscope updateInterval : " + Input.gyro.updateInterval);
	//		GUILayout.Label ("Gyroscope userAcceleration : " + Input.gyro.userAcceleration);
			GUILayout.Label("Gyroscope euler : " + Input.gyro.attitude.eulerAngles);
			GUILayout.Label("gyroinitial euler : " + gyroInitialRotation.eulerAngles);
			GUILayout.Label("initial euler : " + initialRotation.eulerAngles);

			GUILayout.Label("Compass Enabled : " + Input.compass.enabled);

			GUILayout.Label("Compass Heading : " + -Input.compass.trueHeading + "(" + Input.compass.headingAccuracy + ")");
			GUILayout.Label("Initialized Gyro and Campass : " + initialized);

		}
		catch{
		}
	}
	
	// Update is called once per frame

	Vector3 euler;
	public Quaternion quat;
	float roll, pitch, yaw;


	bool initialized = false;


	void Update () {

		if (!initialized)
			return;

		#if !UNITY_EDITOR
		Quaternion offsetRotation = Quaternion.Euler(90, -180, 0) * 	Input.gyro.attitude * Quaternion.Euler(0, 0, 180);

		#else
		Quaternion offsetRotation = Quaternion.Euler(transform.localEulerAngles + 50 * Time.deltaTime * new Vector3(-Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), 0));
		#endif

		transform.localRotation = offsetRotation;
	}
		
}
