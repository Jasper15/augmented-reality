﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SFXItem{
	public string name;
	public AudioClip clip;
}

public class SFXPlayer : MonoBehaviour {

	public List<SFXItem> clips;

	AudioSource source;
 
	private static SFXPlayer _instance;
	public static SFXPlayer Instance{
		get
		{
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<SFXPlayer>();

			return _instance;
		}
	}

	void Awake(){
		source = GetComponent<AudioSource>();
	}

	public void PlayOneShot(string name){
		PlayOneShot(name, 1, 1);
	}

	public void PlayOneShot(string name, float volume = 1, float pitch = 1){
		SFXItem item = clips.Find(i=>i.name == name);

		if(item != null){
			source.volume = volume;
			source.pitch = pitch;
			source.PlayOneShot(item.clip);
		}
	}


}
