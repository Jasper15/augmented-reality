﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using AssetBundles;

public class ButtonMenuState : MonoBehaviour {

	UIRoundRect roundRect;
	RectTransform rt;
	MainUI mainUI;

	public Animator parentPanel;
	public RectTransform thisPanel;
	public PanelUI panelUI;

	public string sceneAssetBundle = "", levelName = "";

	// Use this for initialization
	void Start () {
		mainUI = MainUI.Instance;
		roundRect = GetComponent<UIRoundRect>();
		rt = GetComponent<RectTransform>();
	}
		

	public void PressClose(){
		if (mainUI.State == MenuState.Transition)
			return;

		if (mainUI.State == MenuState.Preview)
		{
			mainUI.StateChanging(MenuState.Carousel);

			parentPanel.SetTrigger("Exit");
			parentPanel.ResetTrigger("Enter");

			panelUI.resize = true;

			StartCoroutine(waitThenCallback(0.5f, () =>
				{ 
					mainUI.StateChanged(MenuState.Carousel);
				}));
		}
	}

	public void PressPlay()
	{
		if (mainUI.State == MenuState.Transition)
			return;
		
		if (mainUI.State == MenuState.Carousel)
		{

			mainUI.StateChanging(MenuState.Preview);

			parentPanel.SetTrigger("Enter");
			parentPanel.ResetTrigger("Exit");

			thisPanel.SetAsLastSibling();
			panelUI.resize = false;


			StartCoroutine(waitThenCallback(0.5f, () =>
				{ 
					mainUI.StateChanged(MenuState.Preview);
				}));
		}

		else if (mainUI.State == MenuState.Preview)
		{

			//loading screen
			StartCoroutine("LoadScene");
			LoadingUI.Instance.Show();
		}
		


	}

	public IEnumerator LoadScene(){
		yield return AssetBundleManager.LoadLevelAsync(sceneAssetBundle, levelName, false);
	}

	public IEnumerator waitThenCallback(float time, System.Action callback)
	{
		yield return new WaitForSeconds(time);

		callback();
	}
}
