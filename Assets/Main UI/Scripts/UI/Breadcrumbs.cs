﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;

public class Breadcrumbs : MonoBehaviour {

	public Color selectedColor = Color.green, unselectedColor = Color.white;

	public List<Image> crumbs = new List<Image>();
	HorizontalLayoutGroup layout;

	public Transform scrollContent;

	int lastSelectIndex = 0;

	// Use this for initialization
	void Awake () {

		layout = GetComponent<HorizontalLayoutGroup>();

		InitChildren(scrollContent.childCount);
	}

	public void InitChildren(int count){
		Clear();
		for (int i = 1; i < count; i++)
		{
			Image crumb = Instantiate(crumbs[0], transform);
			crumb.color = unselectedColor;
			crumbs.Add(crumb);

		}

		layout.enabled = false;
		layout.enabled = true;

		crumbs[0].color = selectedColor;
	}

	public void Select(int index){
		//unselect last
		if (lastSelectIndex == index)
			return;

		UnSelect(lastSelectIndex);

		Image crumb = crumbs[index];

		crumb.color = selectedColor;

		lastSelectIndex = index;

		SFXPlayer.Instance.PlayOneShot("Select", 0.5f, 2);
	}

	void UnSelect(int index){
		Image crumb = crumbs[index];
		crumb.color = unselectedColor;

	}


	void Clear(){
		List<GameObject> deleteList = new List<GameObject>();;
		for (int i = 1; i < transform.childCount; i++)
		{
			deleteList.Add(transform.GetChild(i).gameObject);
		}

		foreach (GameObject crumb in deleteList)
		{
			Destroy(crumb);
		}
	}

}
