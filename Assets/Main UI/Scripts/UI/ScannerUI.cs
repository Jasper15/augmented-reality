﻿using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections.Generic;
using AssetBundles;

using Wizcorp.Utils.Logger;

public class ScannerUI : MonoBehaviour {

	private IScanner BarcodeScanner;
	public Text TextHeader;
	public RawImage Image;
	public Text ScannerText;

	public UICircle ScanTimer;

	public List<GameObject> hideOnScan;

	public Button ScanButton;

	// Disable Screen Rotation on that screen
	void Awake()
	{
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;


	}

	bool scanSuccess = false;
	string stringFound = "";

	void OnEnable () {
		// Create a basic scanner
		BarcodeScanner = new Scanner();
		BarcodeScanner.Camera.Play();

		// Display the camera texture through a RawImage
		BarcodeScanner.OnReady += (sender, arg) => {
			// Set Orientation & Texture
			Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
			Image.transform.localScale = BarcodeScanner.Camera.GetScale();
			Image.texture = BarcodeScanner.Camera.Texture;

			// Keep Image Aspect Ratio
			var rect = Image.GetComponent<RectTransform>();
			var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
			rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
		};

		// Track status of the scanner
		BarcodeScanner.StatusChanged += (sender, arg) => {
			TextHeader.text = "Status: " + BarcodeScanner.Status;
		};

		ScannerText.text = "Scan iAR Code";
	}
		

	/// <summary>
	/// The Update method from unity need to be propagated to the scanner
	/// </summary>
	void Update()
	{
		if (BarcodeScanner == null)
		{
			return;
		}
		BarcodeScanner.Update();
	}

	#region UI Buttons

	public void ClickStart()
	{
		if (BarcodeScanner == null)
		{
			Log.Warning("No valid camera - Click Start");
			return;
		}

		ScannerText.text = "   Scanning...";


		// Start Scanning
		BarcodeScanner.Scan((barCodeType, barCodeValue) => {
			BarcodeScanner.Stop();
			TextHeader.text = "Found: " + barCodeType + " / " + barCodeValue;
			ScannerText.text = "Scan Successful";

			scanSuccess = true;
			stringFound = barCodeValue;

			// Feedback
			SFXPlayer.Instance.PlayOneShot("Scan Success");

			#if UNITY_ANDROID || UNITY_IOS
			Handheld.Vibrate();
			#endif
		});

		//Stop Scanning after x seconds
		float duration = 3 * 160f/60;
		StartCoroutine(waitThenCallback(duration, () =>
			{ 
				if(scanSuccess && stringFound != ""){
					string sceneBundleName = GetBundleName(stringFound);
					if(sceneBundleName != "" && AssetBundleManager.Instance.isValidSceneBundle(sceneBundleName)){
						//found a scene
						TextHeader.text = sceneBundleName + " is a valid scene.";
						string sceneName = sceneBundleName.Replace("scenes/", "");

						LoadingUI.Instance.Show();
						StartCoroutine(LoadScene(sceneBundleName, sceneName));

						return;
					}

					else{
						TextHeader.text = sceneBundleName + " is invalid.";
					}
				}

				ClickStop();
				SFXPlayer.Instance.PlayOneShot("Scan Error", 0.5f, 1.5f);
				ScannerText.text = "iAR Code not found";

			}));

		StartCoroutine(ScanTime( duration));
		StartCoroutine("PlayScanTune");
	}


	public string GetBundleName(string barcodeString){
		if (!barcodeString.Contains("-"))
			return "";
		
		return barcodeString.Split(new char[]{'-'})[1];
	}

	public IEnumerator PlayScanTune(){
		yield return new WaitForSeconds(20f/60);
		for (int i = 0; i < 12; i++)
		{
			SFXPlayer.Instance.PlayOneShot("Scan Tone", 0.5f, 2f);
			yield return new WaitForSeconds(40f/60);

		}
	}

	public IEnumerator ScanTime(float duration){
		float startTime = Time.time;

		foreach (GameObject g in hideOnScan)
		{
			g.SetActive(false);
		}
		ScanButton.interactable = false;

		while (Time.time - startTime < duration)
		{
			ScanTimer.fillPercent = 100 * (Time.time - startTime) / duration;
			ScanTimer.gameObject.SetActive(false);
			ScanTimer.gameObject.SetActive(true);

			yield return new WaitForEndOfFrame();
		}

		ScanTimer.fillPercent = 100;
		ScanTimer.gameObject.SetActive(false);
		ScanTimer.gameObject.SetActive(true);


		foreach (GameObject g in hideOnScan)
		{
			g.SetActive(true);
		}

		ScanButton.interactable = true;


		yield break;
		
	}

	public void ClickStop()
	{
		if (BarcodeScanner == null)
		{
			Log.Warning("No valid camera - Click Stop");
			return;
		}

		// Stop Scanning
		BarcodeScanner.Stop();

	}

	public void StopAndExit()
	{
		ClickStop();
		ClickBack();
	}

	public void ClickBack()
	{
		// Try to stop the camera before loading another scene
		StartCoroutine(StopCamera(() => {
			//SceneManager.LoadScene("Boot");
		}));
	}

	/// <summary>
	/// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
	/// Trying to stop the camera in OnDestroy provoke random crash on Android
	/// </summary>
	/// <param name="callback"></param>
	/// <returns></returns>
	public IEnumerator StopCamera(Action callback)
	{
		// Stop Scanning
		//Image = null;
		BarcodeScanner.Destroy();
		BarcodeScanner = null;

		// Wait a bit
		yield return new WaitForSeconds(0.1f);

		callback.Invoke();
	}

	public IEnumerator waitThenCallback(float time, System.Action callback)
	{
		yield return new WaitForSeconds(time);

		callback();
	}

	public IEnumerator LoadScene(string sceneAssetBundle, string levelName ){
		yield return AssetBundleManager.LoadLevelAsync(sceneAssetBundle, levelName, false);
	}

	#endregion
}
