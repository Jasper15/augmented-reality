﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Events;


/// <summary>
/// Unity VideoPlayer Script for Unity 5.6 (currently in beta 0b11 as of March 15, 2017)
/// Blog URL: http://justcode.me/unity2d/how-to-play-videos-on-unity-using-new-videoplayer/
/// YouTube Video Link: https://www.youtube.com/watch?v=nGA3jMBDjHk
/// StackOverflow Disscussion: http://stackoverflow.com/questions/41144054/using-new-unity-videoplayer-and-videoclip-api-to-play-video/
/// Code Contiburation: StackOverflow - Programmer
/// </summary>

[RequireComponent(typeof(VideoPlayer))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(RawImage))]
public class VideoUI : MonoBehaviour {

	private RawImage image;

	public VideoClip videoToPlay;

	private VideoPlayer videoPlayer;
	private VideoSource videoSource;

	private AudioSource audioSource;

	public Button playButton;

	public UnityEvent videoFinished;


	// Use this for initialization
	void Start () {
		Application.runInBackground = true;

		image = GetComponent<RawImage>();
		videoPlayer = GetComponent<VideoPlayer>();
		audioSource = GetComponent<AudioSource>();

		StartCoroutine("PlayVideo");

	}

	public void TogglePlay(){
		if (videoPlayer.isPlaying)
		{
			Debug.Log("Toggle Play Video");
			videoPlayer.Play();
			audioSource.Play();
		}
		else
		{
			Debug.Log("Toggle Pause Video");
			videoPlayer.Pause();
			audioSource.Pause();
		}
			
	}

	public void Play(){
		StartCoroutine("PlayRoutine");
	}

	public IEnumerator PlayRoutine(){

		Debug.Log("Play Video");
		videoPlayer.enabled = false;
		videoPlayer.enabled = true;
		videoPlayer.Play();
		audioSource.Play();

		while (!videoPlayer.isPlaying)
		{
			yield return new WaitForEndOfFrame();
		}

		while (videoPlayer.isPlaying)
		{
			//Debug.Log("Video Time: " + videoPlayer.time);
			yield return new WaitForEndOfFrame();
		}


		Debug.Log("Play Video Finished");
		videoFinished.Invoke();
	}

	public void Pause(){
		Debug.Log("Pause Video");
		videoPlayer.Pause();
		audioSource.Pause();
	}

	IEnumerator PlayVideo()
	{

		//Disable Play on Awake for both Video and Audio
		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;
		audioSource.Pause();

		//We want to play from video clip not from url

		videoPlayer.source = VideoSource.VideoClip;

		// Vide clip from Url
		//videoPlayer.source = VideoSource.Url;
		//videoPlayer.url = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";


		//Set Audio Output to AudioSource
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

		//Assign the Audio from Video to AudioSource to be played
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(0, audioSource);

		//Set video To Play then prepare Audio to prevent Buffering
		videoPlayer.clip = videoToPlay;
		videoPlayer.Prepare();

		//Wait until video is prepared
		while (!videoPlayer.isPrepared)
		{
			yield return new WaitForEndOfFrame();
		}

		Debug.Log("Done Preparing Video");

		//Assign the Texture from Video to RawImage to be displayed
		image.texture = videoPlayer.texture;

		Debug.Log("Video Ready");

		if (playButton != null)
		{
			playButton.gameObject.SetActive(true);
		}
		else
		{	
			Debug.Log("AutoPlay Video");
			//Play Video
			videoPlayer.Play();

			//Play Sound
			audioSource.Play();

			while (videoPlayer.isPlaying)
			{
				Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
				yield return new WaitForEndOfFrame();
			}
		}
	}

	// Update is called once per frame
	void Update () {

	}
}
