﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelUI : MonoBehaviour {

	public bool resize = true;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if(!resize){
			transform.localScale = Vector3.one;
		}

		else{
			float r = Mathf.Abs(transform.position.x + 97.5f) / 1000;
			transform.localScale = (1 - r) * Vector3.one;
		}
	}
}
