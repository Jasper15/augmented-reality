﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUIAnimator : MonoBehaviour {

	public Animator TopBar, CarouselArrows, ScanButton;

	void OnEnable(){
		MainUI.evStateChanging += MainUI_evStateChanging;	
	}
	void OnDisable(){
		MainUI.evStateChanging -= MainUI_evStateChanging;	
	}
	void MainUI_evStateChanging (MenuState state)
	{
		if (state == MenuState.Preview)
		{
			Hide(TopBar);
			Hide(CarouselArrows);
			Hide(ScanButton);
		}

		else if (state == MenuState.Carousel)
		{
			Show(TopBar);
			Show(CarouselArrows);
			Show(ScanButton);
		}
	}

	void Hide (Animator a){
		a.SetTrigger ("Hide");
		a.ResetTrigger ("Show");
	}
	void Show (Animator a){
		a.SetTrigger("Show");
		a.ResetTrigger("Hide");
	}

}
