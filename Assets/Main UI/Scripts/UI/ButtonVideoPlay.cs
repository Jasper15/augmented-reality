﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class ButtonVideoPlay : MonoBehaviour {

	UIRoundRect roundRect;
	RectTransform rt;
	MainUI mainUI;

	public Animator parentPanel;
	public RectTransform thisPanel;
	public PanelUI panelUI;

	bool allowInputs = true;

	// Use this for initialization
	void Start () {
		mainUI = MainUI.Instance;
		roundRect = GetComponent<UIRoundRect>();
		rt = GetComponent<RectTransform>();
	}

	public void ButtonPressed(){

		if (!allowInputs)
			return;

		allowInputs = false;
		mainUI.StateChanging(MenuState.Preview);

		parentPanel.SetTrigger("Enter");
		parentPanel.ResetTrigger("Exit");

		thisPanel.SetAsLastSibling();
		panelUI.resize = false;


		StartCoroutine(waitThenCallback(0.5f, () =>
			{ 
				mainUI.StateChanged(MenuState.Preview);
				gameObject.SetActive(false);
				allowInputs = true;
			}));
	}

	public IEnumerator waitThenCallback(float time, System.Action callback)
	{
		yield return new WaitForSeconds(time);

		callback();
	}
}
