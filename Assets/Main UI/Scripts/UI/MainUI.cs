﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MenuState {Carousel, Preview, Transition}

public class MainUI : MonoBehaviour {

	public delegate void MenuStateEvent (MenuState state);
	public static event MenuStateEvent evStateChanging, evStateChanged;

	private MenuState _state = MenuState.Carousel;

	private static MainUI _instance;

	public static MainUI Instance{
		get{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<MainUI>();
			}
			return _instance;
		}
	}


	public void StateChanging(MenuState state){
		_state = MenuState.Transition;

		if(evStateChanging != null)
			evStateChanging(state);
	}
	public void StateChanged(MenuState state){
		_state = state;

		if(evStateChanged != null)
			evStateChanged(state);
	}
	public MenuState State{
		get{
			return _state;
		}
	}

}
