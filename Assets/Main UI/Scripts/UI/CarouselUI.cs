﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;


public class CarouselUI : MonoBehaviour {

	ScrollRect scrollRect;
	ScrollSnap scrollSnap;
	Transform carouselContent;

	void OnEnable(){
		MainUI.evStateChanging += MainUI_evStateChanging;
	}
	void OnDisable(){
		MainUI.evStateChanging -= MainUI_evStateChanging;
	}

	void MainUI_evStateChanging (MenuState state)
	{
		if (state == MenuState.Carousel)
		{
			scrollRect.enabled = true;
			scrollSnap.allowInputs = true;
			resizeChildren = true;
		}
		else if (state == MenuState.Preview)
		{
			scrollRect.enabled = false;
			scrollSnap.allowInputs = false;
			resizeChildren = false;
		}
	}

	// Use this for initialization
	void Start () {
		scrollRect = GetComponent<ScrollRect>();
		scrollSnap = GetComponent<ScrollSnap>();
		carouselContent = scrollRect.content.transform;
	}

	bool resizeChildren = true;

	// Update is called once per frame
	void Update () {
		
	}

}
