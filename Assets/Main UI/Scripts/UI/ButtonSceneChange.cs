﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class ButtonSceneChange : MonoBehaviour {
	private CanvasGroup ui;
	public void GoToScene(string sceneName){
		SceneManager.LoadScene(sceneName);
	}
}
