﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingUI : MonoBehaviour {

	private static LoadingUI _instance;

	public static LoadingUI Instance
	{
		get{
			if(_instance == null){
				_instance = GameObject.FindObjectOfType<LoadingUI>();
			}
			return _instance;
		}	
	}

	Animator loadingUIAnim;
	bool loading = false;

	void Start(){
		loadingUIAnim = GetComponent<Animator>();


		DontDestroyOnLoad(gameObject);
	}

	void OnLevelWasLoaded(){
		if (loading)
		{
			Hide();
		}
	}

	public void Show(){
		loadingUIAnim.SetTrigger("Show");
		loading = true;
	}

	public void Hide(){
		loadingUIAnim.SetTrigger("Hide");
		loading = false;
	}
}
