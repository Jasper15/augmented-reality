﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSelect : MonoBehaviour {

	public int foodId;
	public Animator anim;
	public GameObject burger, sausage, steak;

	private GameObject foodOptions;

	void Awake()
	{
		foodOptions = gameObject.transform.parent.gameObject;
	}

	void OnMouseDown()
	{
		anim.SetInteger ("id", foodId);
		foodOptions.SetActive (false);
		if (foodId == 1)
		{
			burger.SetActive (true);
		}
		else if (foodId == 2)
		{
			steak.SetActive (true);
		}
		else{
			sausage.SetActive (true);
		}
			
	}
}
