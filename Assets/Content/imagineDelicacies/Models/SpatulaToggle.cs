﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpatulaToggle : MonoBehaviour {

	public GameObject spatula, burger, sausage,steak,foodButtons;

	public void Off()
	{
		spatula.SetActive (false);
	}

	public void On()
	{
		spatula.SetActive (true);
	}

	void OnDisable(){
		spatula.SetActive (false);
		burger.SetActive (false);
		steak.SetActive (false);
		sausage.SetActive (false);
		foodButtons.SetActive (true);
	}


}
