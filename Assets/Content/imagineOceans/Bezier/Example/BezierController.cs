﻿/**
 * Author: Sander Homan
 * Copyright 2012
 **/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//
class BezierController : MonoBehaviour
{
    public BezierPath path = null;
    public float speed = 1;
    public bool byDist = false;

	public bool lookforward = true;

    private float t = 0;

    void Start()
    {
    }

	Vector3 oldPos, newPos;


    void Update()
    {
        t += speed*Time.deltaTime;
      
		oldPos = transform.position;
		newPos = path.GetPositionByT(t);

		if (lookforward) {
			float angle = Mathf.Atan2 (newPos.z - oldPos.z, newPos.x - oldPos.x);
			angle += transform.eulerAngles.y;             
			//transform.eulerAngles = new Vector3(0,angle,0);
        
			transform.forward = (newPos - oldPos);
		}
			
		transform.position = newPos;
       
    }
}

