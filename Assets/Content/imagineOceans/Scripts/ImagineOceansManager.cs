﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace imagineAR.ImagineOceans
{
	public class ImagineOceansManager : MonoBehaviour {

		public float startDelay = 5, waveDelay = 3, intervalDelay = 0.5f; 
		public List<GameObject> trashes;
		public List<string> message;
		//We only have one ocean;

		public Transform world;

		public int waveCount = 1, maxWaveCount = 5;

		bool gameOver = false;

		public Image vignette, fadeToBlack;

		public float currentLife = 10, maxLife = 10, damage = 1;

		public Renderer skySphere, seaSphere;

		public Color targetSkyCol, targetSeaCol;
		Color skyCol, seaCol;

		public ParticleSystem explode;
		float startTime = 0;
		public void Play(){
			startTime = Time.time;
			startGame = true;
		}


		public GameObject playbutton;

		IEnumerator ShowPlayButton(){
			yield return new WaitForSeconds(60);
			playbutton.SetActive(true);
		}

		void OnEnable(){
			Trash.evTrashTap += Trash_evTrashTap;
			Trash.evTrashSettle += Trash_evTrashSettle;

			skyCol = skySphere.material.color;
			seaCol = seaSphere.material.color;

		}

		void OnDisable(){
			Trash.evTrashTap -= Trash_evTrashTap;
			Trash.evTrashSettle -= Trash_evTrashSettle;


			skySphere.material.color = skyCol;
			seaSphere.material.color = seaCol;


		}

		void Trash_evTrashTap (GameObject g)
		{
			ParticleSystem explodeCopy = Instantiate(explode, g.transform.position, Quaternion.identity);
			explodeCopy.Play();
			AutoDestroyObject.AutoDestroy(explodeCopy.gameObject, explodeCopy.duration);

			SFXPlayer.Instance.PlayOneShot("Click");

			if (currentLife >= maxLife)
			{
				currentLife = maxLife;
				return;
			}

			currentLife += damage;

			float r = currentLife / maxLife;
			Color c = vignette.color;
			c.a = 1 - r;

			vignette.color = c;

			fadeToBlack.color = new Color(0, 0, 0, c.a * c.a * c.a);

			skySphere.material.color = skyCol * r + targetSkyCol * (1-r);
			seaSphere.material.color = seaCol * r + targetSeaCol * (1-r);


		}

		void Trash_evTrashSettle(GameObject g){

			if (currentLife <= 0)
			{
				currentLife = 0;
				return;
			}

			currentLife -= damage;

			float r = currentLife / maxLife;

			Color c = vignette.color;
			c.a = 1 - r;
			vignette.color = c;

			fadeToBlack.color = new Color(0, 0, 0, c.a * c.a * c.a - 0.05f);

			skySphere.material.color = skyCol * r + targetSkyCol * (1-r);
			seaSphere.material.color = seaCol * r + targetSeaCol * (1-r);

			if (currentLife <= 0)
			{
				gameOver = true;
			}
		}


		public bool startGame = false;

		// Use this for initialization
		IEnumerator Start () {
			StartCoroutine("ShowPlayButton");

			while(!startGame){
				yield return new WaitForEndOfFrame();
			}

			yield return new WaitForSeconds(startDelay);

			StartCoroutine("IncreaseDifficulty");


			while (!gameOver)
			{

				for (int i = 0; i < waveCount; i++)
				{
					SpawnTrash();

				

					yield return new WaitForSeconds(intervalDelay);

				}

				yield return new WaitForSeconds(waveDelay);

			}

			StartCoroutine("ShowMessages");
		}

		IEnumerator IncreaseDifficulty (){
			while(true)
			{
				yield return new WaitForSeconds(15);

				foreach (GameObject trash in trashes)
				{
					trash.GetComponent<Trash>().speed *= 1.25f;

				}

				waveDelay *= 0.75f;

				if (intervalDelay <= 0.15f)
					intervalDelay -= 0.05f;

				waveCount++;
			}

			yield break;
		}
		
		void SpawnTrash(){
			GameObject prefab = trashes[Random.Range(0, trashes.Count)];
			GameObject g = Instantiate(prefab, 
				new Vector3(Random.Range(-50, 50), 200, Random.Range(-50 - 50 ,50 - 50)), 
				Quaternion.Euler(new Vector3(Random.Range(0,360), Random.Range(0,180), Random.Range(0,360))), 
				world);
			g.SetActive(true);

		}


		public Animator messageAnim;
		public Text messageText;

		IEnumerator ShowMessages(){

			yield return new WaitForSeconds(3);

			for (int i = 0; i < message.Count; i++)
			{	
				messageText.text = message[i];
				messageAnim.SetTrigger("Show");
				yield return new WaitForSeconds(4f);
				messageAnim.SetTrigger("Hide");
				yield return new WaitForSeconds(1.5f);
			}

			StartCoroutine("ShowPlasticWorld");
		}

		public GameObject plasticWorld;

		IEnumerator ShowPlasticWorld(){
			plasticWorld.SetActive(true);
			gameObject.SetActive(false);

			fadeToBlack.color = new Color(0, 0, 0, 0);
			vignette.color = new Color(vignette.color.r, vignette.color.g, vignette.color.b, 1);

			yield break;
		}
	}
}