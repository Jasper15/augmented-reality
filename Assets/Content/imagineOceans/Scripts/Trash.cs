﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace imagineAR.ImagineOceans
{
	public class Trash : MonoBehaviour {

		public enum trashState {midair, submerged, settled}

		public delegate void TrashEvent (GameObject g);
		public static event TrashEvent evTrashTap;
		public static event TrashEvent evTrashSettle;

		public Material blackMat;

		public float speed = 10;
		float targetHeight = 0;

		public ParticleSystem bubbles, ripple;

		trashState state = trashState.midair;

		public bool dontDestroy = false;

		Vector3 startEul;

		void Start(){
			targetHeight = Random.Range(-10, 10);
			startEul = transform.eulerAngles;
		}


		void OnMouseDown(){
			if (dontDestroy)
				return;

			Destroy(gameObject);

			if (evTrashTap != null)
				evTrashTap(gameObject);
		}

		// Update is called once per frame
		void Update () {

			transform.eulerAngles = new Vector3(
				startEul.x + 20 * Mathf.Sin(Time.time + targetHeight), 
				startEul.y, 
				startEul.z + 20 * Mathf.Sin(Time.time + targetHeight)
			);

			if (transform.localPosition.y > 80)
			{
				transform.localPosition += (5 * speed * Time.deltaTime * Vector3.down);
			}

			else if (transform.localPosition.y < targetHeight)
			{
				//stay here
				if (evTrashSettle != null && state != trashState.settled)
				{
					evTrashSettle(gameObject);
					state = trashState.settled;
				}
			}

			else
			{
				if (state == trashState.midair )
				{
					state = trashState.submerged;
					bubbles.gameObject.SetActive(true);
					ripple.gameObject.SetActive(true);
					GetComponent<Renderer>().material = blackMat;

					SFXPlayer.Instance.PlayOneShot("Splash" + Random.Range(0, 3));
				}
				transform.localPosition += (1 * speed * Time.deltaTime * Vector3.down);

			}
		}
	}
}
