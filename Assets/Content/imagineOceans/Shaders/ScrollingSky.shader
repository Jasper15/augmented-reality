﻿Shader "imagine/ScrollingSky"
{
	Properties
	{
	 _Color ("Tint", Color) = (1, 1, 1, 1)
		_MainTex ("Texture", 2D) = "white" {}
		_VelX ("Velocity X", float) = 10
		_VelY ("Velocity Y", float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Lighting Off
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Color;
			half _VelX;
			half _VelY;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float2 scrollUV = i.uv;
			 scrollUV.x = scrollUV.x + _Time * _VelX;
			 scrollUV.y = scrollUV.y + _Time * _VelY;

				// sample the texture
				fixed4 col = tex2D(_MainTex, scrollUV) * _Color;

				return col;
			}
			ENDCG
		}
	}
}
