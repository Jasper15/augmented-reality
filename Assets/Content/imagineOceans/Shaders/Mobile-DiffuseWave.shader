// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Mobile/Diffuse Wave" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_FX ("X", Float) = 2.0
    _FZ ("Z", Float) = 4.0
     _TMul ("TimeMult", Float) = 50
    _TOff ("TimeOffset", Float) = 0
     _Mag ("Magnitude", Float) = 0.1
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd vertex:vert


sampler2D _MainTex;
 float _FX;
								float _FZ;
								float _TOff;
								float _TMul;
								float _Mag;

struct Input {
	float2 uv_MainTex;
};

void vert (inout appdata_full v) {
			 float angle = _Time * _TMul + _TOff;
           
    v.vertex.y += v.texcoord.x * sin(v.vertex.x / _FX + angle);
    v.vertex.y += sin(v.vertex.z / _FZ + angle);
}

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
