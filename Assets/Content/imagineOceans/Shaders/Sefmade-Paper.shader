﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'glstate.matrix.mvp' with 'UNITY_MATRIX_MVP'

Shader "Selfmade/Paper"
{
 
Properties
{
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Texture", 2D) = "white" { }
    _FX ("X", Float) = 2.0
    _FZ ("Z", Float) = 4.0
     _TMul ("imeMult", Float) = 50
    _TOff ("TimeOffset", Float) = 0
     _Mag ("Magnitude", Float) = 0.1

}
 
SubShader
{


    Pass
    {
        CULL Off
       
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"
        #include "AutoLight.cginc"
       
        float4 _Color;
        sampler2D _MainTex;

        float _FX;
								float _FZ;
								float _TOff;
								float _TMul;
								float _Mag;

       
        //float4 _Time;
       
        // vertex input: position, normal
        struct appdata {
            float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
        };
       
        struct v2f {
            float4 pos : POSITION;
            float2 uv: TEXCOORD0;
        };
       
        v2f vert (appdata v) {
            v2f o;
           
            float angle= _Time * _TMul + _TOff;
           
            v.vertex.y +=  v.texcoord.x * sin(v.vertex.x / _FX + angle);
            v.vertex.y += sin(v.vertex.z / _FZ + angle);
            v.vertex.y *= v.vertex.x * _Mag;
           
            o.pos = UnityObjectToClipPos( v.vertex );
            o.uv = v.texcoord;
            return o;
        }
       
        float4 frag (v2f i) : COLOR
        {
            half4 color = tex2D(_MainTex, i.uv);
            return color;
        }
 
        ENDCG
       
 
        //SetTexture [_MainTex] {combine texture}
    }
}
Fallback "VertexLit"
}