﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ButtonController : MonoBehaviour {
	public GameObject roofButton;
	public GameObject secondFloorButton;
	public GameObject secondFloor;
	// Update is called once per frame
	void Update () {
		if (secondFloorButton.active == true) {
			roofButton.GetComponent<Button> ().interactable = false;
		}
		else roofButton.GetComponent<Button> ().interactable = true;

		if (roofButton.active == false) {
			secondFloor.GetComponent<BoxCollider>().enabled = false;
		}
		else secondFloor.GetComponent<BoxCollider>().enabled = true;
	}
}
