﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Tap : MonoBehaviour {

	public GameObject objectButton;

	void OnMouseDown() {
		this.gameObject.SetActive (false);
		this.objectButton.SetActive (true);
	}

	public void Show() {
		this.gameObject.SetActive (true);
		this.objectButton.SetActive (false);
	}
}
