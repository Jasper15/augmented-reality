﻿using UnityEngine;
#if UNITY_EDITOR	
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//using Helpers;
/*
 	In this demo, we demonstrate:
	1.	Automatic asset bundle dependency resolving & loading.
		It shows how to use the manifest assetbundle like how to get the dependencies etc.
	2.	Automatic unloading of asset bundles (When an asset bundle or a dependency thereof is no longer needed, the asset bundle is unloaded)
	3.	Editor simulation. A bool defines if we load asset bundles from the project or are actually using asset bundles(doesn't work with assetbundle variants for now.)
		With this, you can player in editor mode without actually building the assetBundles.
	4.	Optional setup where to download all asset bundles
	5.	Build pipeline build postprocessor, integration so that building a player builds the asset bundles and puts them into the player data (Default implmenetation for loading assetbundles from disk on any platform)
	6.	Use WWW.LoadFromCacheOrDownload and feed 128 bit hash to it when downloading via web
		You can get the hash from the manifest assetbundle.
	7.	AssetBundle variants. A prioritized list of variants that should be used if the asset bundle with that variant exists, first variant in the list is the most preferred etc.
*/

namespace AssetBundles
{	

	// Loaded assetBundle contains the references count which can be used to unload dependent assetBundles automatically.
	public class LoadedAssetBundle
	{
		public AssetBundle m_AssetBundle;
		public int m_ReferencedCount;
		
		public LoadedAssetBundle(AssetBundle assetBundle)
		{
			m_AssetBundle = assetBundle;
			m_ReferencedCount = 1;
		}
	}
	
	// Class takes care of loading assetBundle and its dependencies automatically, loading variants automatically.
	public class AssetBundleManager : MonoBehaviour
	{
		public enum LogMode { All, JustErrors };
		public enum LogType { Info, Warning, Error };
	
        static AssetBundleManager m_Instance;

		static LogMode m_LogMode = LogMode.All;
		static string m_BaseDownloadingURL = "";
		static string[] m_ActiveVariants =  {  };
		static AssetBundleManifest m_AssetBundleManifest = null;
	#if UNITY_EDITOR	
		static int m_SimulateAssetBundleInEditor = -1;
		const string kSimulateAssetBundles = "SimulateAssetBundles";
	#endif
	
		static Dictionary<string, LoadedAssetBundle> m_LoadedAssetBundles = new Dictionary<string, LoadedAssetBundle> ();
		
        //static Dictionary<string, WWW> m_DownloadingWWWs = new Dictionary<string, WWW> ();
		//static List<AssetBundleLoadOperation> m_InProgressOperations = new List<AssetBundleLoadOperation> ();
		
        static KeyValuePair<string, WWW> m_DownloadingWWW;
		static KeyValuePair<string, string> m_DownloadingError = new KeyValuePair<string,string>("", "");
		static AssetBundleLoadOperation m_InProgressOperation, m_FinishedOperation;

        static Dictionary<string, string[]> m_Dependencies = new Dictionary<string, string[]> ();

		public static int m_DownloadedBundles, m_TotalBundles;
	

		public delegate void AssetbundleManagerEvent();

		public static event AssetbundleManagerEvent evDownloadFailedDueToError;

        public static AssetBundleManager Instance{
            get{
				if (m_Instance == null)
				{
					//Look for AssetbundleManager in the scene
					m_Instance = GameObject.FindObjectOfType<AssetBundleManager>();

					//Create a new AssetbundleManager if it doesn't exist
					if (m_Instance == null)
					{
						Debug.Log("Creating new ABM");

						var go = new GameObject("AssetBundleManager", typeof(AssetBundleManager));
						DontDestroyOnLoad(go);
						m_Instance = go.GetComponent<AssetBundleManager>();
					}
					else
					{
						Debug.Log("Found ABM with name " + m_Instance.gameObject.name);
					}
				}
                return m_Instance;
            }
        }

        public static LogMode logMode
		{
			get { return m_LogMode; }
			set { m_LogMode = value; }
		}
	
		// The base downloading url which is used to generate the full downloading url with the assetBundle names.
		public static string BaseDownloadingURL
		{
			get { return m_BaseDownloadingURL; }
			set { m_BaseDownloadingURL = value; }
		}
	
		// Variants which is used to define the active variants.
		public static string[] ActiveVariants
		{
			get { return m_ActiveVariants; }
			set { m_ActiveVariants = value; }
		}
	
		// AssetBundleManifest object which can be used to load the dependecies and check suitable assetBundle variants.
		public static AssetBundleManifest AssetBundleManifestObject
		{
			get {return m_AssetBundleManifest; }
			set {m_AssetBundleManifest = value; }
		}
	
		private static void Log(LogType logType, string text)
		{
			if (logType == LogType.Error)
				Debug.LogError("[AssetBundleManager] " + text);
			else if (m_LogMode == LogMode.All)
				Debug.Log("[AssetBundleManager] " + text);
		}
	

		public static string GetDownloadingBundle(){
			if (m_DownloadingWWW.Value != null)
			{
				return m_DownloadingWWW.Key;
			}
			else
			{
				return "";
			}
		}

		public static float GetBundleLoadingProgress(){
			if (m_DownloadingWWW.Value != null)
			{
				return m_DownloadingWWW.Value.progress;
			}
			else
			{
				return 0;
			}
		}

		public static float GetTotalDownloadProgress(){
			float increment = 0;
			if (m_DownloadingWWW.Value != null)
			{
				increment = m_DownloadingWWW.Value.progress;
			}
				
			return (float)(m_DownloadedBundles + increment) / m_TotalBundles;
		}
			

	#if UNITY_EDITOR
		// Flag to indicate if we want to simulate assetBundles in Editor without building them actually.
		public static bool SimulateAssetBundleInEditor 
		{
			get
			{
				if (m_SimulateAssetBundleInEditor == -1)
					m_SimulateAssetBundleInEditor = EditorPrefs.GetBool(kSimulateAssetBundles, true) ? 1 : 0;
				
				return m_SimulateAssetBundleInEditor != 0;
			}
			set
			{
				int newValue = value ? 1 : 0;
				if (newValue != m_SimulateAssetBundleInEditor)
				{
					m_SimulateAssetBundleInEditor = newValue;
					EditorPrefs.SetBool(kSimulateAssetBundles, value);
				}
			}
		}
		
	
		#endif
	
		private static string GetStreamingAssetsPath()
		{
			if (Application.isEditor)
				return "file://" +  System.Environment.CurrentDirectory.Replace("\\", "/"); // Use the build output folder directly.
			else if (Application.isWebPlayer)
				return System.IO.Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/")+ "/StreamingAssets";
			else if (Application.isMobilePlatform || Application.isConsolePlatform)
				return Application.streamingAssetsPath;
			else // For standalone player.
				return "file://" +  Application.streamingAssetsPath;
		}
	
		public static void SetSourceAssetBundleDirectory(string relativePath)
		{
			BaseDownloadingURL = GetStreamingAssetsPath() + relativePath;
		}
		
		public static void SetSourceAssetBundleURL(string absolutePath)
		{
			BaseDownloadingURL = absolutePath + Utility.GetPlatformName() + "/";
		}
	
		public static void SetDevelopmentAssetBundleServer()
		{
			#if UNITY_EDITOR
			// If we're in Editor simulation mode, we don't have to setup a download URL
			if (SimulateAssetBundleInEditor)
				return;
			#endif
			
			TextAsset urlFile = Resources.Load("AssetBundleServerURL") as TextAsset;
			string url = (urlFile != null) ? urlFile.text.Trim() : null;
			if (url == null || url.Length == 0)
			{
				Debug.LogError("Development Server URL could not be found.");
				//AssetBundleManager.SetSourceAssetBundleURL("http://localhost:7888/" + UnityHelper.GetPlatformName() + "/");
			}
			else
			{
				AssetBundleManager.SetSourceAssetBundleURL(url);
			}
		}
		
		// Get loaded AssetBundle, only return vaild object when all the dependencies are downloaded successfully.
		static public LoadedAssetBundle GetLoadedAssetBundle (string assetBundleName, out string error)
		{
			error = "";

			if (m_DownloadingError.Key == assetBundleName)
			{
				error = m_DownloadingError.Value;	
				return null;
			}
		
			LoadedAssetBundle bundle = null;
			m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);
			if (bundle == null)
				return null;
			
			// No dependencies are recorded, only the bundle itself is required.
			string[] dependencies = null;
			if (!m_Dependencies.TryGetValue(assetBundleName, out dependencies) )
				return bundle;
			
			// Make sure all dependencies are loaded
			foreach(var dependency in dependencies)
			{
				if (m_DownloadingError.Key == assetBundleName ){
					error = m_DownloadingError.Value;
					return bundle;
				}
				// Wait all the dependent assetBundles being loaded.
				LoadedAssetBundle dependentBundle;
				m_LoadedAssetBundles.TryGetValue(dependency, out dependentBundle);
				if (dependentBundle == null)
					return null;
			}
	
			return bundle;
		}
	
		static public IEnumerator Initialize ()
		{
			yield return Instance.StartCoroutine( Initialize(Utility.GetPlatformName()));
		}
			
	
		// Load AssetBundleManifest.
        static public IEnumerator Initialize (string manifestAssetBundleName)
		{
	#if UNITY_EDITOR
			Log (LogType.Info, "Simulation Mode: " + (SimulateAssetBundleInEditor ? "Enabled" : "Disabled"));
	#endif
	

		
	#if UNITY_EDITOR	
			// If we're in Editor simulation mode, we don't need the manifest assetBundle.
			if (SimulateAssetBundleInEditor)
                yield break;
	#endif
	
			yield return Instance.StartCoroutine( LoadAssetBundle(manifestAssetBundleName, true));
			var operation = new AssetBundleLoadManifestOperation (manifestAssetBundleName, "AssetBundleManifest", typeof(AssetBundleManifest));
            m_InProgressOperation = operation;

			while (!operation.IsDone())
            {
                yield return new WaitForEndOfFrame ( );
            }

        }

		//recursive function which gets all bundles to download
		static List<string> GetBundlesToDownload (string assetBundleName, List<string> bundleList){

			if(!bundleList.Contains(assetBundleName))
				bundleList.Add(assetBundleName);

			List<string> dependencies = m_AssetBundleManifest.GetAllDependencies (assetBundleName).ToList();
			foreach (string d in dependencies)
			{
				if (!bundleList.Contains(d))
				{
					bundleList.Add(d);
				}
			}
			return bundleList;
		}

		// Load AssetBundle and its dependencies.
        static protected IEnumerator LoadAssetBundle(string assetBundleName, bool isLoadingAssetBundleManifest = false)
		{
			Log(LogType.Info, "Loading Asset Bundle " + (isLoadingAssetBundleManifest ? "Manifest: " : ": ") + assetBundleName);
	
	#if UNITY_EDITOR
			// If we're in Editor simulation mode, we don't have to really load the assetBundle and its dependencies.
			if (SimulateAssetBundleInEditor)
                yield break;
	#endif
	


			// Load dependencies.
			if (!isLoadingAssetBundleManifest)
			{



				//Calculate download size
				m_TotalBundles = GetBundlesToDownload(assetBundleName, new List<string>()).Count;
				Debug.Log("download size for " + assetBundleName + " is " + m_TotalBundles);
				m_DownloadedBundles = 0;

				yield return Instance.StartCoroutine(LoadDependencies(assetBundleName));


			}

			// Check if the assetBundle has already been processed.
			//bool isAlreadyProcessed = LoadAssetBundleInternal(assetBundleName, isLoadingAssetBundleManifest);

			if (CheckDownloadForErrors() != "")
			{
				//dependency has already failed. Abort download.
				yield break;
			}
			yield return Instance.StartCoroutine(LoadAssetBundleInternal(assetBundleName, isLoadingAssetBundleManifest));
			m_DownloadedBundles++; //increment counter for main bundle;

		}

		static AssetBundleManifest TryLoadingManifestBackUp(string manifestFileName){
#if UNITY_EDITOR
			string path = Application.dataPath + "/" + manifestFileName;
#else
			string path = Application.persistentDataPath + "/" + manifestFileName;
#endif
			if (System.IO.File.Exists(path))
			{
				Debug.Log("Fetching manifest backup from " + path);

				byte[] bytes = System.IO.File.ReadAllBytes(path);
				AssetBundle manifestBundle =  AssetBundle.LoadFromMemory(bytes, 0);
				return manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
			}
			else
			{
				Debug.Log("Back up for manifest file not found");
				return null;
			}
		}

		static void SaveManifestBackUp(WWW download, string manifestFileName){
			//Save a copy of manifest as backup
#if UNITY_EDITOR
			string path = Application.dataPath + "/" + manifestFileName;
#else
			string path = Application.persistentDataPath + "/" + manifestFileName;
#endif
			Debug.Log("Saving manifest backup to " + path);

			System.IO.File.WriteAllBytes(path, download.bytes);
		}

		// Remaps the asset bundle name to the best fitting asset bundle variant.
		static protected string RemapVariantName(string assetBundleName)
		{
			string[] bundlesWithVariant = m_AssetBundleManifest.GetAllAssetBundlesWithVariant();

			Debug.Log(assetBundleName);
			string[] split = assetBundleName.Split('.');

			int bestFit = int.MaxValue;
			int bestFitIndex = -1;
			// Loop all the assetBundles with variant to find the best fit variant assetBundle.
			for (int i = 0; i < bundlesWithVariant.Length; i++)
			{
				string[] curSplit = bundlesWithVariant[i].Split('.');
				if (curSplit[0] != split[0])
					continue;
				
				int found = System.Array.IndexOf(m_ActiveVariants, curSplit[1]);
				
				// If there is no active variant found. We still want to use the first 
				if (found == -1)
					found = int.MaxValue-1;
						
				if (found < bestFit)
				{
					bestFit = found;
					bestFitIndex = i;
				}
			}
			

			if (bestFit == int.MaxValue-1)
			{
				Debug.LogWarning("Ambigious asset bundle variant chosen because there was no matching active variant: " + bundlesWithVariant[bestFitIndex]);
			}

			if (bestFitIndex != -1)
			{
				return bundlesWithVariant[bestFitIndex];
			}
			else
			{
				return assetBundleName;
			}
		}
	
		// Where we actuall call WWW to download the assetBundle.
        static protected IEnumerator LoadAssetBundleInternal (string assetBundleName, bool isLoadingAssetBundleManifest)
		{
			// Already loaded.
			LoadedAssetBundle bundle = null;
			m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);
			if (bundle != null)
			{
				bundle.m_ReferencedCount++;
				//return true;
                yield break;
			}
	
			// @TODO: Do we need to consider the referenced count of WWWs?
			// In the demo, we never have duplicate WWWs as we wait LoadAssetAsync()/LoadLevelAsync() to be finished before calling another LoadAssetAsync()/LoadLevelAsync().
			// But in the real case, users can call LoadAssetAsync()/LoadLevelAsync() several times then wait them to be finished which might have duplicate WWWs.
            if (m_DownloadingWWW.Key == assetBundleName)
            {
                Debug.Log("Downloading " + assetBundleName + " has already started. Please wait for it to finish");
                yield break;
            }

			string url = m_BaseDownloadingURL + assetBundleName;
		
            Debug.Log("Downloading " + url);

			// For manifest assetbundle, always download it as we don't have hash for it.
	
            using (WWW download = (isLoadingAssetBundleManifest ? new WWW (url) : WWW.LoadFromCacheOrDownload (url, m_AssetBundleManifest.GetAssetBundleHash (assetBundleName), 0)))
            {    
                    m_DownloadingWWW = new KeyValuePair<string, WWW>(assetBundleName, download);
                    yield return download;
            }
	
			yield break;
		}
	
		// Where we get all the dependencies and load them all.
        static protected IEnumerator LoadDependencies(string assetBundleName)
		{
			if (m_AssetBundleManifest == null)
			{
				Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
                yield break;
			}
	
			// Get dependecies from the AssetBundleManifest object..
			string[] dependencies = m_AssetBundleManifest.GetAllDependencies(assetBundleName);
			if (dependencies.Length == 0)
                yield break;
			
			for (int i=0;i<dependencies.Length;i++)
				dependencies[i] = RemapVariantName (dependencies[i]);
				
			// Record and load all dependencies.
			m_Dependencies.Add(assetBundleName, dependencies);
			for (int i = 0; i < dependencies.Length; i++)
			{
				if (CheckDownloadForErrors() != "")
				{
					//a dependency has already failed. Abort download
					yield break;
				}

				//yield return new WaitForSeconds(1);
				yield return Instance.StartCoroutine(LoadAssetBundleInternal(dependencies[i], false));
				m_DownloadedBundles++; //increment counter for each dependency loaded
			}
		}
	
		// Unload assetbundle and its dependencies.
		static public void UnloadAssetBundle(string assetBundleName)
		{
	#if UNITY_EDITOR
			// If we're in Editor simulation mode, we don't have to load the manifest assetBundle.
			if (SimulateAssetBundleInEditor)
				return;
	#endif
	
			//Debug.Log(m_LoadedAssetBundles.Count + " assetbundle(s) in memory before unloading " + assetBundleName);
	
			UnloadAssetBundleInternal(assetBundleName);
			UnloadDependencies(assetBundleName);
	
			//Debug.Log(m_LoadedAssetBundles.Count + " assetbundle(s) in memory after unloading " + assetBundleName);
		}
	
		static protected void UnloadDependencies(string assetBundleName)
		{
			string[] dependencies = null;
			if (!m_Dependencies.TryGetValue(assetBundleName, out dependencies) )
				return;
	
			// Loop dependencies.
			foreach(var dependency in dependencies)
			{
				UnloadAssetBundleInternal(dependency);
			}
	
			m_Dependencies.Remove(assetBundleName);
		}
	
		static protected void UnloadAssetBundleInternal(string assetBundleName)
		{
			string error;
			LoadedAssetBundle bundle = GetLoadedAssetBundle(assetBundleName, out error);
			if (bundle == null)
				return;
	
			if (--bundle.m_ReferencedCount == 0)
			{
				bundle.m_AssetBundle.Unload(false);
				m_LoadedAssetBundles.Remove(assetBundleName);
	
				Log(LogType.Info, assetBundleName + " has been unloaded successfully");
			}
		}
	
		void Update()
		{
			// Collect the finished WWW.

            WWW download = m_DownloadingWWW.Value;
            string key = m_DownloadingWWW.Key;

			if (download != null)
			{
				// If downloading fails.
				if (download.error != null)
				{
					string errorMsg = string.Format("Failed downloading bundle {0} from {1}: {2}", key, download.url, download.error);
					m_DownloadingError = new KeyValuePair<string,string>(key, errorMsg);
					Debug.LogError("<color=red>" + errorMsg + "</color>");
					TerminateDueToError();

					RemoveDownload();
				}
		
				// If downloading succeeds.
				else if (download.isDone)
				{
					AssetBundle bundle = download.assetBundle;
					if (bundle == null)
					{
						string errorMsg = string.Format("{0} does not exist.", key);
						m_DownloadingError = new KeyValuePair<string,string>(key, errorMsg);
						Debug.LogError("<color=red>" + errorMsg + "</color>");
						TerminateDueToError();
					}
					else
					{
						Debug.Log(string.Format("<color=green>[{0}/{1}] {2} downloaded successfully.</color>", m_DownloadedBundles, m_TotalBundles, key));
						m_LoadedAssetBundles.Add(key, new LoadedAssetBundle(download.assetBundle));

//						//hack save manifest
						if(m_DownloadingWWW.Key == Utility.GetPlatformName())
							SaveManifestBackUp(m_DownloadingWWW.Value, Utility.GetPlatformName());

					}	
					RemoveDownload();
				}
			}
	
	
			// Update operation in progress
            if (m_InProgressOperation != null && !m_InProgressOperation.Update())
			{
                 m_InProgressOperation = null;
			}
		}
	
        static void RemoveDownload(){



			m_DownloadingWWW.Value.Dispose();
			m_DownloadingWWW = new KeyValuePair<string, WWW>("", null);

			//m_DownloadedBundles++;
        }

		static public AssetBundleLoadOperation FinishedOperation{
			get{
				return m_FinishedOperation;
			}
		}

		// Load asset from the given assetBundle.
		static public IEnumerator LoadAssetAsync (string assetBundleName, string assetName, System.Type type)
		{
			Log(LogType.Info, "Loading " + assetName + " from " + assetBundleName + " bundle");
	
			AssetBundleLoadAssetOperation operation = null;
	#if UNITY_EDITOR
			if (SimulateAssetBundleInEditor)
			{
				string[] assetPaths = AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(assetBundleName, assetName);
				if (assetPaths.Length == 0)
				{
					Debug.LogError("There is no asset with name \"" + assetName + "\" in " + assetBundleName);
					yield break;
				}
	
				// @TODO: Now we only get the main object from the first asset. Should consider type also.
				Object target = AssetDatabase.LoadMainAssetAtPath(assetPaths[0]);
				operation = new AssetBundleLoadAssetOperationSimulation (target);
			}
			else
	#endif
			{
				assetBundleName = RemapVariantName (assetBundleName);
				yield return Instance.StartCoroutine( LoadAssetBundle (assetBundleName));
				operation = new AssetBundleLoadAssetOperationFull (assetBundleName, assetName, type);
	
				m_InProgressOperation = operation;

				while (!operation.IsDone())
				{
					yield return new WaitForEndOfFrame ( );
				}

				m_FinishedOperation = operation;
			}
	
		}
	
		// Load level from the given assetBundle.
        static public IEnumerator LoadLevelAsync (string assetBundleName, string levelName, bool isAdditive)
		{
			Log(LogType.Info, "Loading " + levelName + " from " + assetBundleName + " bundle");
	
			AssetBundleLoadOperation operation = null;
//	#if UNITY_EDITOR
//			if (SimulateAssetBundleInEditor)
//			{
//				operation = new AssetBundleLoadLevelSimulationOperation(assetBundleName, levelName, isAdditive);
//			}
//			else
//	#endif
			{
                //no variant name for scene assetbundle
				//assetBundleName = RemapVariantName(assetBundleName);
                Debug.Log("assetBundle name remapped = " + assetBundleName);
				yield return Instance.StartCoroutine( LoadAssetBundle (assetBundleName));
				operation = new AssetBundleLoadLevelOperation (assetBundleName, levelName, isAdditive);
	
                m_InProgressOperation = operation;
            }
	
			while (!operation.IsDone())
            {
                yield return new WaitForEndOfFrame ( );
            }

			Debug.Log("done loading scene");

			m_FinishedOperation = operation;
		}


		public static string CheckDownloadForErrors(){
			return m_DownloadingError.Value;
		}



		public static void TerminateDueToError(){
			//Instance.StopAllCoroutines();


			if (evDownloadFailedDueToError != null)
				evDownloadFailedDueToError();

			//Backup plan - try loading Manifest from old copy
			if (m_AssetBundleManifest == null)
			{
				//to keep downloads as stable as possible
				//even without internet connection
				//try looking for a cached copy of the manifest
				m_AssetBundleManifest = TryLoadingManifestBackUp(Utility.GetPlatformName());

				if (m_AssetBundleManifest == null)
				{
					string errorMsg = "Still can't find cached copy of Manifest.";
					Debug.LogError(errorMsg);
					return;
				}
				else
				{
					Debug.Log("Found cached copy of manifest. Yay!");
				}


			}
		}

		public static void Reset(){
			m_DownloadingWWW = new KeyValuePair<string, WWW>("", null);
			m_InProgressOperation = null;
			m_FinishedOperation = null;
			m_DownloadingError = new KeyValuePair<string, string>("", "");
			m_Dependencies.Clear();
		}

		public static AssetBundleManifest GetAssetBundleManifest(){
			return m_AssetBundleManifest;
		}

		public List<string> GetSceneBundles(){
			List<string> scenesBundles = m_AssetBundleManifest.GetAllAssetBundles().ToList();
			scenesBundles = scenesBundles.FindAll(s => s.Contains("scenes/"));
			return scenesBundles;
		}

		public bool isValidSceneBundle(string sceneBundle){
			return GetSceneBundles().Contains(sceneBundle);
		}

	} // End of AssetBundleManager.
}