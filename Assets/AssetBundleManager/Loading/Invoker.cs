﻿using UnityEngine;
using System.Collections;

public class Invoker : MonoBehaviour {

	private static Invoker _instance;

	public static Invoker Instance{
		get{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<Invoker>();
			}
			return _instance;
		}
	}

	public IEnumerator waitThenCallback(float time, System.Action callback)
	{
		yield return new WaitForSeconds(time);
		callback();
	}
}
