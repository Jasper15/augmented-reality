﻿using UnityEngine;
using System.Collections;
using AssetBundles;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class LoadPanels : MonoBehaviour {

	//public string assetBundleName;
	//public string panelName;


	public LoadingBar loadingBar;
	public RectTransform loadingPanel;

	public GameObject swipe;

	IEnumerator Start ()
	{
		yield return StartCoroutine(Initialize() );

//		//get manifest from asset bundle manager
//		AssetBundleManifest manifest = AssetBundleManager.GetAssetBundleManifest();
//		if(manifest == null){
//			Debug.Log("Didn't get list of panels. Manifest is null");
//			yield break;
//		}
//			
//
//		//get panels from manifest
//		//bundle format for a panel asset bundle "panels/<PANEL NAME>/prefab"
//		foreach (string s in manifest.GetAllAssetBundles())
//		{
//			Debug.Log(s);
//		}
//		List<string> panels = manifest.GetAllAssetBundles().ToList().FindAll(s => (s.StartsWith("panels/") && s.EndsWith("/prefab")));
//		Debug.Log("Found " + panels.Count + " panels to download");
//
//		//initialize loading bar with number of panels to download
//		loadingBar.downloadedBundles = 0;
//		loadingBar.totalBundles = panels.Count;
//
//		//start download our panels
//		foreach (string panel in panels)
//		{
//			string panelName = panel.Replace("panels/", "");
//			panelName = panelName.Replace("/prefab", "");
//			Debug.Log("Downloading " + panelName + "...");
//
//			//wait for panel asset bundle download (and its corresponding dependencies) to finish
//			yield return StartCoroutine(InstantiateGameObjectAsync (panel, panelName) );
//
//			loadingBar.downloadedBundles++;
//		}
//
//
//		RearrangeChildren();
//
//		//set download to 100%
//		loadingBar.SetFill(1);
//
//		yield return new WaitForSeconds(0.1f);
//
//		//initialize scrolling
//		//GameObject.FindObjectOfType<ScrollRectSnapV2>().Initialize();
//
//		//set scrolling anchors
//		RectTransform rt = GetComponent<RectTransform>();
//		rt.anchorMax = new Vector2(transform.childCount, 1);
//		rt.sizeDelta = Vector2.zero;
//
//		//enable swipe indicator
//		if (transform.childCount > 1)
//		{
//			Invoker i = Invoker.Instance;
//			i.StartCoroutine(i.waitThenCallback(5, () =>
//					{
//					swipe.gameObject.SetActive(true);
//				
//				}));
//		}
//			
//
//		//deactivate loading bar
//		if (loadingBar != null)
//			loadingBar.gameObject.SetActive(false);
	}

	// Initialize the downloading url and AssetBundleManifest object.
	protected IEnumerator Initialize()
	{
		// Don't destroy this gameObject as we depend on it to run the loading script.
		DontDestroyOnLoad(gameObject);

		// With this code, when in-editor or using a development builds: Always use the AssetBundle Server
		// (This is very dependent on the production workflow of the project. 
		// 	Another approach would be to make this configurable in the standalone player.)
		#if DEVELOPMENT_BUILD || UNITY_EDITOR
		AssetBundleManager.SetDevelopmentAssetBundleServer ();
		#else
		// Use the following code if AssetBundles are embedded in the project for example via StreamingAssets folder etc:
		//AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + "/");
		AssetBundleManager.SetSourceAssetBundleURL("http://imagesblues.com/app/bundles/1.1/");

		// Or customize the URL based on your deployment or configuration
		//AssetBundleManager.SetSourceAssetBundleURL("http://www.MyWebsite/MyAssetBundles");
		#endif

		// Initialize AssetBundleManifest which loads the AssetBundleManifest object.
		yield return AssetBundleManager.Initialize();

	}

	protected IEnumerator InstantiateGameObjectAsync (string assetBundleName, string assetName)
	{
		// This is simply to get the elapsed time for this phase of AssetLoading.
		float startTime = Time.realtimeSinceStartup;

		AssetBundleLoadAssetOperation request = null;
		// Load asset from assetBundle.
		yield return AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject) );

		request = (AssetBundleLoadAssetOperation)AssetBundleManager.FinishedOperation;

		if (request == null)
		{
			yield break;

		}

		// Get the asset.
		GameObject prefab = request.GetAsset<GameObject> ();

		if (prefab != null)
		{
			GameObject g = (GameObject)GameObject.Instantiate(prefab, transform);
			//g.transform.parent = transform;
			RectTransform rt = g.GetComponent<RectTransform>();
			rt.anchoredPosition3D = new Vector3(g.transform.GetSiblingIndex() * 1152 + 576, 0, 0);


			rt.GetChild(0).GetComponent<RectTransform>().sizeDelta = loadingPanel.GetChild(0).GetComponent<RectTransform>().sizeDelta;
			rt.localScale = Vector3.one;
			rt.sizeDelta = loadingPanel.sizeDelta;

		}

		yield return new WaitForEndOfFrame();


		// Calculate and display the elapsed time.
		float elapsedTime = Time.realtimeSinceStartup - startTime;
		Debug.Log(assetName + (prefab == null ? " was not" : " was")+ " loaded successfully in " + elapsedTime + " seconds" );
	}


//	public List<ScrollPanel> panels;
	void RearrangeChildren(){

		//Get children
//		panels = transform.GetComponentsInChildren<ScrollPanel>().ToList();
//		panels = panels.OrderBy(o => o.priority).ToList();
//
//		for (int index = 1; index < panels.Count; index++)
//		{
//			
//
//			RectTransform containerRT = panels[index].transform.parent.GetComponent<RectTransform>();
//			RectTransform panelRT = panels[index].GetComponent<RectTransform>();
//
//			containerRT.SetSiblingIndex(index);
//
//			containerRT.anchoredPosition3D = new Vector3(index * 1152 + 576, 0, 0);
//
//			panelRT.sizeDelta = loadingPanel.GetChild(0).GetComponent<RectTransform>().sizeDelta;
//			containerRT.localScale = Vector3.one;
//			containerRT.sizeDelta = loadingPanel.sizeDelta;
//		}
			
	}
}
