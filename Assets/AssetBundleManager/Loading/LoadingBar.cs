﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AssetBundles;
using UnityEngine.UI.Extensions;

public class LoadingBar : MonoBehaviour {

	//public Image mainLoadingBar;

	float progress;


	float step;

	UIImageCrop barCrop;

	// Update is called once per frame
	void OnEnable(){
		barCrop = GetComponent<UIImageCrop>();

		progress = 0;
		//mainLoadingBar.material.SetFloat("_Fill", 0);
		barCrop.SetXCrop(0);
	}

	void Update () {

		if (AssetBundleManager.m_TotalBundles == 0)
			return;

		progress = Mathf.Lerp(progress, AssetBundleManager.GetTotalDownloadProgress(), Time.deltaTime * 5);
		progress = AssetBundleManager.GetTotalDownloadProgress();
		progress = progress < 0 ? 0 : progress;




		SetFill(progress);
	}

	public void SetFill(float fill ){
		//mainLoadingBar.material.SetFloat("_Fill", fill );
		barCrop.SetXCrop(fill);

		Debug.Log("XCrop is " + barCrop.XCrop);

	}

	void Exit(){
		Destroy(transform.parent.gameObject);
	}
}
