/// Credit zge
/// Sourced from - http://forum.unity3d.com/threads/draw-circles-or-primitives-on-the-new-ui-canvas.272488/#post-2293224

using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/Primitives/UI RoundRect")]
    public class UIRoundRect : MaskableGraphic
    {
        [SerializeField]
        Texture m_Texture;
        [Range(0, 100)]
        public float fillPercent = 100;
        public bool fill = true;

        public float thickness = 100;

		public float radius = 10;

        [Range(0, 360)]
        public int segments = 360;
     
        public override Texture mainTexture
        {
            get
            {
                return m_Texture == null ? s_WhiteTexture : m_Texture;
            }
        }
     
     
        /// <summary>
        /// Texture to be used.
        /// </summary>
        public Texture texture
        {
            get
            {
                return m_Texture;
            }
            set
            {
                if (m_Texture == value)
                    return;
     
                m_Texture = value;
                SetVerticesDirty();
                SetMaterialDirty();
            }
        }
     
     
        void Update()
        {
            //this.thickness = (float)Mathf.Clamp(this.thickness, 0, rectTransform.rect.width / 2);


			this.radius = Mathf.Clamp(this.radius, 
				0,
				Mathf.Min(rectTransform.rect.width/2,rectTransform.rect.height/2)
					);
			this.thickness = Mathf.Clamp(this.thickness, 0,
				Mathf.Min(rectTransform.rect.width,rectTransform.rect.height)
			);
        }
     
        protected UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs)
        {
            UIVertex[] vbo = new UIVertex[4];
            for (int i = 0; i < vertices.Length; i++)
            {
                var vert = UIVertex.simpleVert;
                vert.color = color;
                vert.position = vertices[i];
                vert.uv0 = uvs[i];
                vbo[i] = vert;
            }
            return vbo;
        }
     
//		public Vector2 pos0 = new Vector2(0,0);
//		public Vector2 pos1 = new Vector2(0,0);
//		public Vector2 pos2 = new Vector2(0,0);
//		public Vector2 pos3 = new Vector2(0,0);

		Vector2 pos0, pos1, pos2, pos3;

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            float outerW = rectTransform.rect.width/2;
			float innerW = (rectTransform.rect.width - this.thickness)/2;

			float outerH = rectTransform.rect.height/2;
			float innerH = (rectTransform.rect.height - this.thickness)/2;
     
            vh.Clear();


     
            Vector2 prevX = Vector2.zero;
            Vector2 prevY = Vector2.zero;
            Vector2 uv0 = new Vector2(0, 0);
            Vector2 uv1 = new Vector2(0, 1);
            Vector2 uv2 = new Vector2(1, 1);
            Vector2 uv3 = new Vector2(1, 0);
       

			//Top
			pos0 = new Vector2(-outerW + radius, outerH);
			pos1 = new Vector2(outerW - radius, outerH);
			pos2 = new Vector2(outerW - radius, innerH);
			pos3 = new Vector2(-outerW + radius, innerH);

			vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));


			//Right
			pos0 = new Vector2(outerW, outerH - radius);
			pos1 = new Vector2(innerW, outerH - radius);
			pos2 = new Vector2(innerW, -outerH + radius);
			pos3 = new Vector2(outerW, -outerH + radius);

			vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));

			//Left
			pos0 = new Vector2(-outerW, outerH - radius);
			pos1 = new Vector2(-innerW, outerH - radius);
			pos2 = new Vector2(-innerW, -outerH + radius);
			pos3 = new Vector2(-outerW, -outerH + radius);

			vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));

			//Bottom
			pos0 = new Vector2(-outerW + radius, -outerH);
			pos1 = new Vector2(outerW - radius, -outerH);
			pos2 = new Vector2(outerW - radius, -innerH);
			pos3 = new Vector2(-outerW + radius, -innerH);


			vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));


     
            float f = 1;
            float degrees = 360f / segments;
            int fa = (int)((segments + 1) * f);
     
			pos0 = Vector2.zero;

			Vector2 offset = Vector2.zero;


            for (int i = 0; i < fa; i++)
            {
				if (i <= 90 / degrees)
					offset = new Vector2(outerW - radius, outerH - radius);
				else if (i <= 180  / degrees)
					offset = new Vector2(-outerW + radius, outerH - radius);
				else if (i <= 270  / degrees)
					offset = new Vector2(-outerW + radius, -outerH + radius);
				else 
					offset = new Vector2(outerW - radius, -outerH + radius);

                float angle = Mathf.Deg2Rad * (i * degrees);
				float c = Mathf.Cos(angle);
				float s = Mathf.Sin(angle);
     
                pos0 = prevX ;
				pos1 = new Vector2(c, s) * radius;
     

				pos2 = new Vector2(c, s) * (radius - .5f * thickness) ;
				pos3 = prevX * (radius - 0.5f * thickness) / radius;
               

     
                prevX = pos1;
                prevY = pos2;
     
				vh.AddUIVertexQuad(
					SetVbo(
						new[] { pos0 + offset, pos1 + offset, pos2 + offset, pos3 + offset }, 
						new[] { uv0, uv1, uv2, uv3 }
					)
				);
     
            }
        }
    }
}